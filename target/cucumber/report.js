$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("PostcodesApi.feature");
formatter.feature({
  "line": 1,
  "name": "Query a postcode and receive a 200 response",
  "description": "",
  "id": "query-a-postcode-and-receive-a-200-response",
  "keyword": "Feature"
});
formatter.before({
  "duration": 15761663900,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Query a postcode and receive a 200 response",
  "description": "",
  "id": "query-a-postcode-and-receive-a-200-response;query-a-postcode-and-receive-a-200-response",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I send a get request to api.postcodes.io/postcodes/SW1P4JA",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "I get a 200 response",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 53
    },
    {
      "val": "4",
      "offset": 55
    }
  ],
  "location": "ApiStepDefinition.i_send_a_get_request_to_api_postcodes_io_postcodes_SW_P_JA(int,int)"
});
formatter.result({
  "duration": 14601393600,
  "error_message": "java.net.ConnectException: Connection refused: connect\r\n\tat java.net.DualStackPlainSocketImpl.connect0(Native Method)\r\n\tat java.net.DualStackPlainSocketImpl.socketConnect(Unknown Source)\r\n\tat java.net.AbstractPlainSocketImpl.doConnect(Unknown Source)\r\n\tat java.net.AbstractPlainSocketImpl.connectToAddress(Unknown Source)\r\n\tat java.net.AbstractPlainSocketImpl.connect(Unknown Source)\r\n\tat java.net.PlainSocketImpl.connect(Unknown Source)\r\n\tat java.net.SocksSocketImpl.connect(Unknown Source)\r\n\tat java.net.Socket.connect(Unknown Source)\r\n\tat org.apache.http.conn.scheme.PlainSocketFactory.connectSocket(PlainSocketFactory.java:121)\r\n\tat org.apache.http.impl.conn.DefaultClientConnectionOperator.openConnection(DefaultClientConnectionOperator.java:180)\r\n\tat org.apache.http.impl.conn.ManagedClientConnectionImpl.open(ManagedClientConnectionImpl.java:326)\r\n\tat org.apache.http.impl.client.DefaultRequestDirector.tryConnect(DefaultRequestDirector.java:610)\r\n\tat org.apache.http.impl.client.DefaultRequestDirector.execute(DefaultRequestDirector.java:445)\r\n\tat org.apache.http.impl.client.AbstractHttpClient.doExecute(AbstractHttpClient.java:835)\r\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:83)\r\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:56)\r\n\tat org.apache.http.client.HttpClient$execute$0.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:148)\r\n\tat io.restassured.internal.RequestSpecificationImpl$RestAssuredHttpBuilder.doRequest(RequestSpecificationImpl.groovy:2055)\r\n\tat io.restassured.internal.http.HTTPBuilder.doRequest(HTTPBuilder.java:495)\r\n\tat io.restassured.internal.http.HTTPBuilder.request(HTTPBuilder.java:452)\r\n\tat io.restassured.internal.http.HTTPBuilder$request$2.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:166)\r\n\tat io.restassured.internal.RequestSpecificationImpl.sendHttpRequest(RequestSpecificationImpl.groovy:1451)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\r\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1262)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1029)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:816)\r\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.callCurrent(PogoInterceptableSite.java:55)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCallCurrent(CallSiteArray.java:51)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:171)\r\n\tat io.restassured.internal.RequestSpecificationImpl.sendRequest(RequestSpecificationImpl.groovy:1200)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\r\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1262)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1029)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:816)\r\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:166)\r\n\tat io.restassured.internal.filter.SendRequestFilter.filter(SendRequestFilter.groovy:30)\r\n\tat io.restassured.filter.Filter$filter$0.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat io.restassured.filter.Filter$filter.call(Unknown Source)\r\n\tat io.restassured.internal.filter.FilterContextImpl.next(FilterContextImpl.groovy:72)\r\n\tat io.restassured.filter.time.TimingFilter.filter(TimingFilter.java:56)\r\n\tat io.restassured.filter.Filter$filter.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:157)\r\n\tat io.restassured.internal.filter.FilterContextImpl.next(FilterContextImpl.groovy:72)\r\n\tat io.restassured.filter.FilterContext$next.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:148)\r\n\tat io.restassured.internal.RequestSpecificationImpl.applyPathParamsAndSendRequest(RequestSpecificationImpl.groovy:1655)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\r\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1262)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1029)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:816)\r\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.callCurrent(PogoInterceptableSite.java:55)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCallCurrent(CallSiteArray.java:51)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:171)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:203)\r\n\tat io.restassured.internal.RequestSpecificationImpl.applyPathParamsAndSendRequest(RequestSpecificationImpl.groovy:1661)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\r\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1262)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1029)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:816)\r\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.callCurrent(PogoInterceptableSite.java:55)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCallCurrent(CallSiteArray.java:51)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:171)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:203)\r\n\tat io.restassured.internal.RequestSpecificationImpl.get(RequestSpecificationImpl.groovy:171)\r\n\tat io.restassured.internal.RequestSpecificationImpl.get(RequestSpecificationImpl.groovy)\r\n\tat io.restassured.RestAssured.get(RestAssured.java:720)\r\n\tat com.stepdef.ApiStepDefinition.i_send_a_get_request_to_api_postcodes_io_postcodes_SW_P_JA(ApiStepDefinition.java:15)\r\n\tat ✽.When I send a get request to api.postcodes.io/postcodes/SW1P4JA(PostcodesApi.feature:5)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 8
    }
  ],
  "location": "ApiStepDefinition.i_get_a_response(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 4598750700,
  "status": "passed"
});
formatter.uri("UKvisa.feature");
formatter.feature({
  "line": 1,
  "name": "Confirm whether a visa is required to visit the UK",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk",
  "keyword": "Feature"
});
formatter.before({
  "duration": 9673863900,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "validate Japan study visa is required",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;validate-japan-study-visa-is-required",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I provide a nationality of Japan for study",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I select the reason “Study”",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I state I am intending to stay for more than 6 months",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I submit the form",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I will be informed “I need a visa to study in the UK”",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_provide_a_nationality_of_Japan_for_study()"
});
formatter.result({
  "duration": 2700821100,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_select_the_reason_Study()"
});
formatter.result({
  "duration": 2703905700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "6",
      "offset": 45
    }
  ],
  "location": "StepDefinitionUKvisa.i_state_I_am_intending_to_stay_for_more_than_months(int)"
});
formatter.result({
  "duration": 517068400,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_submit_the_form()"
});
formatter.result({
  "duration": 2465761200,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_will_be_informed_I_need_a_visa_to_study_in_the_UK()"
});
formatter.result({
  "duration": 89353500,
  "status": "passed"
});
formatter.after({
  "duration": 1240208700,
  "status": "passed"
});
formatter.before({
  "duration": 10283900700,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "validate Japan tourism visa is required",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;validate-japan-tourism-visa-is-required",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I provide a nationality of Japan",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I select the reason “Tourism”",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I Click on Next step",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I will be informed “I won’t need a visa to study in the UK”",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_provide_a_nationality_of_Japan()"
});
formatter.result({
  "duration": 1743445200,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_select_the_reason_Tourism()"
});
formatter.result({
  "duration": 462582400,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_Click_on_Next_step()"
});
formatter.result({
  "duration": 2120457700,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_will_be_informed_I_won_t_need_a_visa_to_study_in_the_UK()"
});
formatter.result({
  "duration": 115284100,
  "status": "passed"
});
formatter.after({
  "duration": 4874369000,
  "status": "passed"
});
formatter.before({
  "duration": 9266227500,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "validate Russia Tourism visa is required",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;validate-russia-tourism-visa-is-required",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "I provide a nationality of Russia",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "I select the reason for “Tourism”",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I state I am not travelling or visiting a partner or family",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I submit the form by clicking Next",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "I will be informed “I need a visa to come to the UK”",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_provide_a_nationality_of_Russia()"
});
formatter.result({
  "duration": 2496011900,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_select_the_reason_for_Tourism()"
});
formatter.result({
  "duration": 2126499700,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_state_I_am_not_travelling_or_visiting_a_partner_or_family()"
});
formatter.result({
  "duration": 356983000,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_submit_the_form_by_clicking_Next()"
});
formatter.result({
  "duration": 2288705400,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitionUKvisa.i_will_be_informed_I_need_a_visa_to_come_to_the_UK()"
});
formatter.result({
  "duration": 198553700,
  "status": "passed"
});
formatter.after({
  "duration": 1670580400,
  "status": "passed"
});
});