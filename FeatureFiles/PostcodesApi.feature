Feature:  Query a postcode and receive a 200 response

Scenario: Query a postcode and receive a 200 response

When I send a get request to api.postcodes.io/postcodes/SW1P4JA
Then I get a 200 response