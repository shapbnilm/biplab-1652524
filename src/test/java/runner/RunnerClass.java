package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(
		features = "FeatureFiles",
		glue = {"com.stepdef"},
				monochrome = true, 
				plugin = {"pretty", "html:target/cucumber", "json: target/cucumber.json", "junit:target/cukes.xml"},
				dryRun = false, 
				strict = false 
		)

public class RunnerClass {

}
