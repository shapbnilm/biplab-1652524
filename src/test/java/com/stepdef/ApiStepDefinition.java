package com.stepdef;

import org.testng.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class ApiStepDefinition {
	Response response;
	@When("^I send a get request to api\\.postcodes\\.io/postcodes/SW(\\d+)P(\\d+)JA$")
	public void i_send_a_get_request_to_api_postcodes_io_postcodes_SW_P_JA(int arg1, int arg2)  {
	    
		response=RestAssured.get("https://api.postcodes.io/postcodes/SW1P4JA");		
		
	}

	@Then("^I get a (\\d+) response$")
	public void i_get_a_response(int arg1)  {
		Assert.assertEquals(response.statusCode(), arg1);
	}

}
