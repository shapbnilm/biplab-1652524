package com.stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utility.Common;

public class StepDefinitionUKvisa extends Common {

	WebDriver driver=null;
	@Before
	public void setUp() {
		driver=launchBrowser();
		openUrl(driver);
		
	}
	@After
	public void tearDown() {
		tearDown(driver);
	}
	
	@Given("^I provide a nationality of Japan for study$")
	public void i_provide_a_nationality_of_Japan_for_study()  {
		Select drpCountry = new Select(driver.findElement(By.name("response")));
		drpCountry.selectByVisibleText("Japan");
		
		driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
	}

	@And("^I select the reason “Study”$")
	public void i_select_the_reason_Study() {
	   driver.findElement(By.id("response-2")).click();
	   driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	}

	@And("^I state I am intending to stay for more than (\\d+) months$")
	public void i_state_I_am_intending_to_stay_for_more_than_months(int arg1)  {
		
		driver.findElement(By.id("response-1")).click();
		
	}

	@When("^I submit the form$")
	public void i_submit_the_form()  {
		driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	}

	@Then("^I will be informed “I need a visa to study in the UK”$")
	public void i_will_be_informed_I_need_a_visa_to_study_in_the_UK()  {
	    
		String title=driver.getTitle();
		System.out.println(title);
		String expected_title = "You’ll need a visa to study in the UK - Check if you need a UK visa - GOV.UK";
		Assert.assertEquals(expected_title, title);
	}

	@Given("^I provide a nationality of Japan$")
	public void i_provide_a_nationality_of_Japan()  {
		Select drpCountry = new Select(driver.findElement(By.name("response")));
		drpCountry.selectByVisibleText("Japan");
		
		driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
	}

	@And("^I select the reason “Tourism”$")
	public void i_select_the_reason_Tourism()  {
		driver.findElement(By.id("response-0")).click();
	}

	@When("^I Click on Next step$")
	public void i_Click_on_Next_step()  {
		driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	   
	}

	@Then("^I will be informed “I won’t need a visa to study in the UK”$")
	public void i_will_be_informed_I_won_t_need_a_visa_to_study_in_the_UK()  {
		String title=driver.getTitle();
		System.out.println(title);
		String expected_title = "You won’t need a visa to come to the UK - Check if you need a UK visa - GOV.UK";
		Assert.assertEquals(expected_title, title);
	}

	@Given("^I provide a nationality of Russia$")
	public void i_provide_a_nationality_of_Russia()  {
		
		Select drpCountry = new Select(driver.findElement(By.name("response")));
		drpCountry.selectByVisibleText("Russia");
		
		driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
	}

	@And("^I select the reason for “Tourism”$")
	public void i_select_the_reason_for_Tourism()  {
		driver.findElement(By.id("response-0")).click();
		driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	}

	@And("^I state I am not travelling or visiting a partner or family$")
	public void i_state_I_am_not_travelling_or_visiting_a_partner_or_family()  {
		driver.findElement(By.id("response-1")).click();
		
	}

	@When("^I submit the form by clicking Next$")
	public void i_submit_the_form_by_clicking_Next()  {
		driver.findElement(By.xpath("//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")).click();
	}

	@Then("^I will be informed “I need a visa to come to the UK”$")
	public void i_will_be_informed_I_need_a_visa_to_come_to_the_UK()  {
		String title=driver.getTitle();
		System.out.println(title);
		String expected_title = "You’ll need a visa to come to the UK - Check if you need a UK visa - GOV.UK";
		Assert.assertEquals(expected_title, title);
	}

	}
	

